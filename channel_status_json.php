<?php
	// report the channel status
	// include database connection class
	require_once ('classes/dbclass.php');
	
	// include PHPMailer class
	require_once ('phpmailer/class.phpmailer.php');	
	
	// Include database connection class
	require_once ('language_config.php.ini');
	
	// include PHPMailer class
	require_once ('functions.php');
	
	
	
	// Create a new db instance.
	$db = new Database();
	$db->query("SELECT id AS channel, status  FROM channels");
	$channel_stauts = $db->getResult();
	
	
	
	foreach($channel_stauts as $item):	
		
		if ($item['status'] == 0):

			
			$channel_code = $item['channel'];

			// check voucher count agenst channel triggers
			
			$db_channel_count = new Database();
			$total_number_of_vouchers_signed = $db_channel_count->count("SELECT count(channel) FROM voucher WHERE channel = '$channel_code' ");
			
			// get the min value of the channel triggers flag.
			$db_channel_trigger_qouta = new Database();	
			$db_channel_trigger_qouta->query("SELECT MIN(flag) AS flag FROM channel_triggers WHERE cid = '$channel_code' AND reached = 0 ");
			
			$flag_total = $db_channel_trigger_qouta->getResult();
			
		
	
		
			if(!$flag_total['flag'] == null):	
	
				if($total_number_of_vouchers_signed >= $flag_total['flag']):
					
					$quota_flag = $flag_total['flag'];
					
					//echo 'Trigger totals: '.$flag_total['flag'].' number of assign vouchers by channel: '.$total_number_of_vouchers_signed.'<br/>';
					
					// MYSQL There have been do channel triggers set for theses channels, so we will close these channels
					$db_close_the_flag = new Database();
					$db_close_the_flag->update("UPDATE channel_triggers SET reached=1 WHERE cid=$channel_code AND flag = $quota_flag");					
					
			
					$db_channel_name = new Database();	
					$db_channel_name->query("SELECT name AS name FROM channels WHERE id = '$channel_code'");
					$channel_name = $db_channel_name->getResult();
					$name = $channel_name['name'];
					
					
					$db_channel_quota = new Database();	
					$db_channel_quota->query("SELECT MAX(flag) AS flag FROM channel_triggers WHERE reached = 1 AND cid = '$channel_code' LIMIT 1");
					$channel_quota = $db_channel_quota->getResult();
					
					$quota = $channel_quota['flag'];
								

					//Channel 1 has reached it\'s quota, number of issued vouchers total $total_number_of_vouchers_signed
					$user_message = str_replace('{CHANNEL}', $name, $admin_message_one);					
					$user_message = str_replace('{TOTAL}', $quota, $user_message);					
					

					admin_email($user_message);
					
				endif;
			else:
				
				// MYSQL There have been do channel triggers set for theses channels, so we will close these channels
				$db_close_the_channel = new Database();
				$db_close_the_channel->update("UPDATE channels SET status=1 WHERE id=$channel_code");
				
				$db_channel_name = new Database();	
				$db_channel_name->query("SELECT name AS name FROM channels WHERE id = '$channel_code'");
				$channel_name = $db_channel_name->getResult();
				$name = $channel_name['name'];					

			
				
				$db_channel_quota = new Database();	
				$db_channel_quota->query("SELECT MAX(flag) AS flag FROM channel_triggers WHERE reached = 1 AND cid = '$channel_code' LIMIT 1");
				$channel_quota = $db_channel_quota->getResult();
				
				$quota = $channel_quota['flag'];
				
				$user_message = str_replace('{CHANNEL}', $name, $admin_message_three);				
				$user_message = str_replace('{TOTAL}', $quota, $user_message);
				
				//echo 'Channel {CHANNEL} has been closed';
				//$user_message = str_replace('{CHANNEL}', $channel_code, $admin_message_three);					
				admin_email($user_message);
							
			endif;
	
		endif;
	
	endforeach;	
	

	echo '{"Status":'.json_encode($channel_stauts).'}';
	
?>