<?php
  /************************************************************\
   *
   *   PHP Script to signup to the voucher Copyright 2012 
   *	  
   *
   *   This script has the following functions
   *
   *   1) Get the post from the form
   *   2) Validate email address for the post, if flase the error report or if true then insert the email address into db
   *   3) Return a message to the user "this could be a string or json"
   *   4) Send an email to the user to inform them of their voucher.
   *
   *   Description:
   *
   *
   *
  \************************************************************/
	
	
	// include database connection class
	require_once ('classes/dbclass.php');
	
	// include email validation class
	require_once ('classes/validate_email_class.php');
	
	// include PHPMailer class
	require_once ('functions.php');
	
	// include PHPMailer class
	require_once ('phpmailer/class.phpmailer.php');	
	
	// Include database connection class
	require_once ('language_config.php.ini');
	

	$db_mysql_escape_string = new Database();
	
	// clean data and mysql_real_escape_string
	$email = $db_mysql_escape_string->clean($_REQUEST['email']);
	$channel_code = $db_mysql_escape_string->clean($_REQUEST['channel']);
	
	
	$firstname = $db_mysql_escape_string->clean($_REQUEST['firstname']);
	$lastname = $db_mysql_escape_string->clean($_REQUEST['lastname']);
	$gender = $db_mysql_escape_string->clean($_REQUEST['gender']);
	
	if(!empty($_REQUEST['opt_in'])){
	    $opt_in = $db_mysql_escape_string->clean($_REQUEST['opt_in']);
	} else {
		$opt_in = 'no';
	}
	
	// new email validation instance
	$validate = new ValidateEmail();		
	$validate->checkEmailDb($email);
	$validate->isEmail($email);
	
	/* ****** WE need to check that the email address isn't already in the database */

	// if $email variable is set:
	if(isset($email)):
		
		// if $email variable is a valide email:
		// =================== check if email in database start
	
		if(!$validate->isEmail($email) == false):
			
			$email = $validate->isEmail($email);
			
			if($validate->checkEmailDb($email) == 0):
				// 'email already in the database';
				//echo '{"SUCCESS":false, "CODE":1, "MESSAGE":'.json_encode($error_msg_one).'}';
				
				// MYSQL 3QUERY check user in db and send email
				$db_check_user = new Database();
				$db_check_user->query("SELECT vcode FROM voucher WHERE email = '$email' LIMIT 1");					
				$user_in_db_check = $db_check_user->getResult();
				
				// Send email if all tests passed
				if(!empty($user_in_db_check)):	
											
					// build the users message for the email body
					$user_message = str_replace('{VCODE}', splitVcode($user_in_db_check['vcode']), $user_message);					
					// Sending the email out to the user
					user_email($firstname, $lastname, $email, $user_message);		
				endif;
				
									
			else:				
				
				//echo 'email not in the database do something';
				// MYSQL QUERY 1 Check to see if the channel is still open, if channel open then add user and send email
				$db_check_channel = new Database();				
				$db_check_channel->query("SELECT status FROM channels WHERE id= $channel_code LIMIT 1");				
				$channel_status = $db_check_channel->getResult();
				
				if($channel_status['status'] == 0):
				
					/* Insert the user into the voucher table and send the voucher email out.				
					// MYSQL 3 QUERY insert the user into the database.*/					
					$db_add_user = new Database();
					$db_add_user->update("UPDATE voucher SET channel = '$channel_code', firstname = '$firstname', lastname = '$lastname', gender = '$gender', email= '$email' , opt_in = '$opt_in' WHERE vcode = (SELECT vcode FROM (SELECT MIN(vcode) as vcode FROM voucher WHERE channel IS NULL) AS tmptable) AND channel IS NULL");
					
					// MYSQL 3QUERY check user in db and send email
					$db_check_user = new Database();
					$db_check_user->query("SELECT vcode FROM voucher WHERE email = '$email' LIMIT 1");					
					$user_in_db_check = $db_check_user->getResult();	

					
					// Send email if all tests passed
					if(!empty($user_in_db_check)):	
												
						// build the users message for the email body
						$user_message = str_replace('{VCODE}', splitVcode($user_in_db_check['vcode']), $user_message);
						
						// Sending the email out to the user
						user_email($firstname, $lastname, $email, $user_message);	
                         //echo '{"SUCCESS":true, "CODE":3, "MESSAGE":}';
						
					else:
						//echo '{"SUCCESS":false, "CODE":3, "MESSAGE":'.json_encode($error_msg_two).'}';		
					endif;
					
					//echo '{"SUCCESS":true, "CODE":4, "MESSAGE":}';
									
				else:
					$error_msg_three = 'channel closed';
					echo '{"SUCCESS":false, "CODE":4, "MESSAGE":'.json_encode($error_msg_three).'}';	
				endif;

			endif;
			
		else:
			// email address not vaild alert
			$error_msg = str_replace('{VAR}', $email, $error_msg_four);
			echo '{"SUCCESS":false, "CODE":5, "MESSAGE":'.json_encode($error_msg).'}';
		endif;
		// =================== check if email in database end
	endif;
	

	
?>