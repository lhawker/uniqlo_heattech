<?php
/************************************************************\
 *
 *   PHP Script to post channel interactions to Copyright 2012 
 *	  
 *
 *   Description:
 *   All the channels will post there score values to this script
 *	 Tis data will then be added to the social_counts table within the db
 *
 *
 *
\************************************************************/
	
	
	// include database connection class
	require_once ('classes/dbclass.php');
	
	// include email validation class
	require_once ('classes/validate_email_class.php');
	
	// include PHPMailer class
	require_once ('functions.php');
		
	
	$db = new Database();
	// that the form has been submitted.	
	if(!empty($_POST['channel'])):
				
		// clean data and mysql_real_escape_string
		$channel_code = strip_tags(addslashes($db->clean($_POST['channel'])));
		$interactions = strip_tags(addslashes($db->clean($_POST['interactions'])));
		$kj = strip_tags(addslashes($db->clean($_POST['kj'])));
		
		$db->update("UPDATE social_counts SET total = (SELECT total FROM (SELECT total as total FROM social_counts WHERE cid='$channel_code') AS tmptable)+$interactions, kj = (SELECT kj FROM (SELECT kj as kj FROM social_counts WHERE cid='$channel_code') AS tmptabl)+$kj WHERE cid='$channel_code'");
		
		echo '{"SUCCESS":true}';	
	else: 
		echo '{"SUCCESS":false}';	
	endif;
	
	
	
?>