<?php
/************************************************************\
 *
 *   PHP Script to update feed on or off Copyright 2012 
 *
 *
\************************************************************/

	// include database connection class
	require_once ('classes/dbclass.php');
	// new db class instance
	$db = new Database();

	if(isset($_POST['switch']) && isset($_POST['hcode'])):
		$camera_switch = strip_tags(addslashes($db->clean($_POST['switch'])));
		$channel = strip_tags(addslashes($db->clean($_POST['hcode'])));
		// 'Start or Stop the camera feed: ';
		$db->update("UPDATE heatstops SET Feed = $camera_switch WHERE id = $channel");

		echo '{"SUCCESS":True}';	
	else: 
		echo '{"SUCCESS":False}';
	endif;

?>

