<?php
/************************************************************\
   *
   *   PHP Script to inset the footstep data Copyright 2012 
  \************************************************************/
// include database connection class
require_once ('classes/dbclass.php');

if(isset($_POST['hcode']) && isset($_POST['footsteps']) && isset($_POST['kj']) ):
	
	$db = new Database();
	$hcode = strip_tags(addslashes($db->clean($_POST['hcode'])));
	$footsteps = strip_tags(addslashes($db->clean($_POST['footsteps'])));	
	$kj = strip_tags(addslashes($db->clean($_POST['kj'])));	

	$db->update("UPDATE heatstops SET Footsteps = (SELECT Footsteps FROM (SELECT Footsteps as Footsteps FROM heatstops WHERE id=$hcode) AS tmptable)+$footsteps, Kj = (SELECT Kj FROM (SELECT Kj as Kj FROM heatstops WHERE id=$hcode) AS tmptable)+$kj WHERE id=$hcode");

	echo '{"SUCCESS":true}';	
else: 
	echo '{"SUCCESS":false}';	

endif;


?>