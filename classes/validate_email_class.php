<?php
	/* 
	 * This is a class to validate the users email address
	 * if email is not valid then return false.
	 * if email address in db then return false.
	 */
  /************************************************************\
  *
  *	  PHP Email address validation Class Copyright 2012 
  *	  
  *
  *	  This file contains email address validation.
  *   Description: This is a class to validate the users email address.
  *
  *
  *
  \************************************************************/

class ValidateEmail {
		
	// function to check if user entered a vaild email address.	
	public function isEmail($email) {	
		//email is not case sensitive make it lower case
		$email =  strtolower(strip_tags($email));	
		//check if email seems valid
		if (preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/", $email)) {	      
		  // emaill address passed validation
		   return $email;
	    }
	    return false;
	}
	
	// function to check if the email address is already in the database.
	public function checkEmailDb($email) {
		$db = new Database();
		$db->query("SELECT email FROM voucher WHERE email = '$email' ");
		$check_email = $db->getResult();

		if (is_array($check_email)):
			if(count($check_email) > 0):
				return false;
			else:
				return true;
			endif;	
		endif;	
	}
	
}
?>