<?php
  /************************************************************\
  *
  *	  PHP Database connection Class Copyright 2012 
  *	  
  *
  *	  This file contains a database connection class.
  *   Description: Contains database connection, result, query
  *   Management functions, input validation
  *
  *   All functions return true if completed
  *   successfully and false if an error
  *   occurred
  *
  *
  \************************************************************/

class Database {

    /*
     * Edit the following variables
     */

// Live DB details
    private $db_host = 'localhost';     // Database Host
    private $db_user = 'flight';          // Username
    private $db_pass = 'MP279xz131';          // Password
    private $db_name = 'flight_uniqlo';          // Database

    /*
     * End edit
     */

    private $con = false;               // Checks to see if the connection is active
    public $result = array();          // Results that are returned from the query

    /*
     * Connects to the database, only one connection
     * allowed
     */

	
    public function connect() {
        if(!$this->con) {
            $myconn = @mysql_connect($this->db_host,$this->db_user,$this->db_pass);
			
            if($myconn) {
                $seldb = @mysql_select_db($this->db_name,$myconn);
                if($seldb) {
                    $this->con = true;
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

	public function clean($val) {
		$this->connect();
		$data = @mysql_real_escape_string($val);
		return $data;
    }



	/*
	 * Custom sql query function to allow for sql queries to be run on the database.
	 */
	public function query($sql) {
		// open the database connection	
		$this->connect();		
		
		// Check to see if the sql query is trying to run a query, which we don't want to run
		/*if ( preg_match( '/^\s*(create|alter|truncate|drop|replace) /i', $sql ) ) {
			die('You do not have permission to run this query: '.$sql.'' . mysql_error());
		}*/
		
		$query = @mysql_query($sql);		
		
		// Print mysql error if there is a problem with the query
		if (!$query) {
			die('Invalid query: ' . mysql_error());
		}
		
		// select query function
		if ( preg_match( '/^\s*(select) /i', $sql ) ) {			
			if($query) {
	            $this->numResults = mysql_num_rows($query);
	            for($i = 0; $i < $this->numResults; $i++)
	            {
	                $r = mysql_fetch_array($query);
	                $key = array_keys($r);
	                for($x = 0; $x < count($key); $x++)
	                {
	                    // Sanitizes keys so only alphavalues are allowed
	                    if(!is_int($key[$x]))
	                    {
	                        if(mysql_num_rows($query) > 1)
	                            $this->result[$i][$key[$x]] = $r[$key[$x]];
	                        else if(mysql_num_rows($query) < 1)
	                            $this->result = null;
	                        else
	                            $this->result[$key[$x]] = $r[$key[$x]];
	                    }
	                }
	            }
	            return true;
	        } else {
	            return false;
	        }
		}				       
		// close the database connection when the query is finished.
		mysql_close();
	}
	
	/*
	 * Custom sql query function to allow for sql queries to be run on the database.
	 */
	public function update($sql) {
		// open the database connection	
		$this->connect();
		$query = @mysql_query($sql);
		if($query) {
           	return true;
        } else {
            return false;
		}
		// close the database connection when the query is finished.
		mysql_close();
	}
	
	/*
	 * Custom sql query function to allow for sql queries to be run on the database.
	 */
	public function count($sql) {
		// open the database connection	
		$this->connect();
		$query = @mysql_query($sql);
		if($query) {
            $this->numResults = mysql_num_rows($query);
            for($i = 0; $i < $this->numResults; $i++)
            {
                $r = mysql_fetch_array($query);
                $key = array_keys($r);
                for($x = 0; $x < count($key); $x++)
                {
                    // Sanitizes keys so only alphavalues are allowed
                    if(!is_int($key[$x]))
                    {
                        if(mysql_num_rows($query) > 1)
                            $this->result[$i][$key[$x]] = $r[$key[$x]];
                        else if(mysql_num_rows($query) < 1)
                            $this->result = null;
                        else
                            $this->result[$key[$x]] = $r[$key[$x]];
                    }
                }
            }
            return $this->result['count(channel)'];
        } else {
            return false;
		}
		// close the database connection when the query is finished.
		mysql_close();
	}

    /*
    * Returns the result set
    */
    public function getResult() {
        return $this->result;
		//return (object) $this->result;
    }
}

//UPDATE voucher set email = "mika@3dwww.com" where id = (select MAX(bu.id) from (select * from voucher) as bu where email is NULL);

?>
