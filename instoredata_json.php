<?php
	
	// Include database connection class
	require_once ('classes/dbclass.php');

	// include PHPMailer class
	require_once ('functions.php');

	// Create a new db instance.
	$db = new Database();
	$db->query("SELECT FORMAT(Footsteps, 0) AS Footsteps , FORMAT(Kj, 0) AS Kj, Feed, Name FROM heatstops WHERE Flagship=false ");
	$heatstops_result = $db->getResult();

	$db_f = new Database();
	$db_f->query("SELECT FORMAT(Footsteps, 0) AS Footsteps, FORMAT(Kj, 0) AS Kj, Feed, Name FROM heatstops WHERE Flagship=true LIMIT 1");
	$flagship_result = $db_f->getResult();

	$heatstops_data = array();

	if(!empty($heatstops_result)):
		foreach($heatstops_result as $key):
			array_push($heatstops_data, $key);		
		endforeach;
	endif;


	// Function to call twitter JSON search and count tweets
	//count_tweets('HEATTECH');
	$tweets = number_format(count_tweets('HEATTECH'));

	$db_fts = new Database();
	$db_fts->query("SELECT SUM(Footsteps) AS total FROM heatstops");
	$heatstops_total = $db_fts->getResult();


	if(!empty($heatstops_total)):
		$fts_totals = number_format($heatstops_total['total']);
	else:
		$fts_totals = '640,568';
	endif;

	$db_et = new Database();
	$db_et->query("SELECT SUM(Kj) AS total FROM heatstops");	
	// redo this to add sum of sinteractions kj
	
	$energy_total = $db_et->getResult();

	if(!empty($energy_total)):
		//$et_totals = number_format($energy_total['total']);
		$et_totals = $energy_total['total'];
	else:
		// set a default value as a full back
		$et_totals = '950,568';
	endif;
	
	// social_counts
	
	
	$db_social_et = new Database();
	$db_social_et->query("SELECT SUM(kj) AS total FROM social_counts");	
	// redo this to add sum of sinteractions kj
	
	$social_energy_total = $db_social_et->getResult();

	if(!empty($social_energy_total)):
		//$social_et_totals = number_format($social_energy_total['total']);
		$social_et_totals = $social_energy_total['total'];		
	else:
		// set a default value as a full back
		$social_et_totals = '950,568';
	endif;

	$energy_totlas = number_format($et_totals + $social_et_totals);

	$db_int = new Database();
	$db_int->query("SELECT sum(total) AS total FROM social_counts");
	
	$int_total = $db_int->getResult();
	

	if(!empty($int_total)):
		$interactions_total = number_format($int_total['total']);
	else:
		// set a default value as a full back
		$interactions_total = '720,558';
	endif;
	
	// $fb_total = count_facebook(5, 'https://graph.facebook.com/?ids=uniqlo.uk');
        $fb_total = count_facebook(5, 'https://graph.facebook.com/?ids=uniqlo.uk');


	$fb_total = number_format($fb_total);
	// this tweet + likes need come back to this
	$tl_totals = number_format($tweets + $fb_total);

	$totals_data = array(
		"Tweets" => "$tweets",
		"Likes" => "$fb_total",
		"TL_totals" => "$tl_totals",
		"Footsteps" => "$fts_totals",
		"Energy" => "$energy_totlas",
		"Interaction" => "$interactions_total",
	);

// print final data as json str
echo '{"Flagship":'.json_encode($flagship_result).', "Heatstops":'.json_encode($heatstops_data).',"Totals":'.json_encode($totals_data).'}'
?>