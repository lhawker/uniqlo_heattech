<?php
/************************************************************\
 *
 *   PHP Script to logout of the admin area Copyright 2012 
 *	  
 *   Description:
 *   This script has the following.
 *
 *   1) session_start() // Start a session.
 *   2) unset() // Unset the username session.
 *   3) header() // Redirect the header to the login page after logout.
 *
 *
\************************************************************/

// Inialize session
	session_start();

// Delete certain session
	unset($_SESSION['username']);
	unset($_SESSION['roles']);
// Delete all session variables
// session_destroy();

// Jump to login page
	header('Location: index.php');

?>
