<?php

/************************************************************\
 *
 *   PHP Script to search the db for the voucher Copyright 2012 
 *	  
 *   Description:
 *   This script has the following.
 *
 *   1) session_start() // Start a session.
 *   2) unset() // Unset the username session.
 *   3) header() // Redirect the header to the login page after logout.
 *   4) require_once() // Include the db class
 *   5) query() // Search db for voucher number.
 *
 *
\************************************************************/

// Include language config < edit this file to change language
require_once ('../language_config.php.ini');

// Include database connection class
require_once ('../classes/dbclass.php');

	$num1 = '';
	$num2 = '';
	$num3 = '';
	$title = '';
    $submit_button_label = $btn_search;
	$error_all = false;

// Inialize session
session_start();

// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['username'])) {
        header('Location: index.php');
}

// Check to see is a search has been submited.
if (isset($_GET['submit'])):
	
	// Show error if user has not entered the data into all inputs 
	if (empty($_GET['num_set_one']) || empty($_GET['num_set_two']) || empty($_GET['num_set_three'])):
		$error_all = true;
		$title = $emsg_one;
		
	
	else:
		
		$num1 = $_GET['num_set_one'];
		$num2 = $_GET['num_set_two'];
		$num3 = $_GET['num_set_three'];

		
		// Create a new db instance.
		$db = new Database();
		// construct the voucher number from users input:
		                
        // clean data and mysql_real_escape_string
        $num_check = strip_tags(addslashes($db->clean($_GET['num_set_one'].$_GET['num_set_two'].$_GET['num_set_three'])));              
                
                
		// make query on the db
		$db->query("SELECT * FROM voucher WHERE vcode = '$num_check'");
		
		// return the db results.
		$result = $db->getResult();
                
		if(empty($result)):
			// No matching number found in the data raise alarm error
			$error_noMatch = true;
            $title = $emsg_two;
            $submit_button_label = $emsg_confirm_voucher_number_is_correct;
		else:
			$match_found = true;
		endif;
		
		// check if the voucher has been redeemed.
		if(isset($match_found) && $match_found == true):
			if($result['email'] == null):
			// Think this should be changed to channel
			// if($result['channel'] == null):
				
				$title = 'Voucher not yet issued to any email address';
				echo '<style>.hide_one {display: none;}</style>';

			elseif($result['redeem'] == '0'):
				$redeemed = false;
			else:				
				$redeemed = true;
                $title = str_replace('{email}', $result['email'],$rmsg_voucher_already_redeemed);                                
			endif;
		endif;
				
	endif;
 
endif;

?>
<html>

<head>
  <title><?php echo $page_title_search;?></title>
  <link id="admin-stylesheet" rel="stylesheet" href="../css/styles.css" type="text/css" />
</head>

<body onload="document.login.num_set_one.focus();">

<?php 
	if($_SESSION['roles'] == 'admin'):
		$sp_menu = '&nbsp;|&nbsp; <a href="users.php">'.$btn_user.'</a>&nbsp;|&nbsp; <a href="API_tests.php">API Tests</a>&nbsp;|&nbsp; <a href="reset_footsteps.php">'.$btn_ftheat.'</a>';
	else:
		$sp_menu = '';
	endif; 
?>
<!-- admin bar html here -->	
<div class="admin_bar">
	<p><?php echo $wmsg_one; ?> <b><?php echo $_SESSION['username']; ?></b>&nbsp;|&nbsp; <a href="logout.php"><?php echo $btn_logout; ?></a>&nbsp;|&nbsp; <a href="analytics.php"><?php echo $btn_analytics; ?></a><?php echo $sp_menu;?></p>
</div>


<div class="content">
	

<h1><?php if ($title===''){
    echo $page_title_search;
} else {
    echo $title;
}
?></h1>

<div style="color: #ccc">valid voucher number: 077-906-392</div>
<form name="login" method="GET" action="<?php echo $_SERVER['PHP_SELF']; ?>" >

	<p>
	<label for="num_set_one">
		<input value="<?php echo $num1; ?>" tabindex="1" accesskey="1" id="num_set_one" type="text" maxlength="3" name="num_set_one" size="3" />
	<label>
	<span>&nbsp;-&nbsp;</span>
	<label for="num_set_two">
		<input value="<?php echo $num2; ?>" tabindex="2" accesskey="2" id="num_set_two" type="text" maxlength="3" name="num_set_two" size="3" />
	<label>
	<span>&nbsp;-&nbsp;</span>
	<label for="num_set_three">
		<input tabindex="3" value="<?php echo $num3; ?>" accesskey="3" id="num_set_three" type="text" maxlength="3" name="num_set_three" size="3" />
	<label>
	</p>

	<input class="form_button" name="submit" tabindex="4" accesskey="s" type="submit" value="<?php echo $submit_button_label; ?>" />
</form>

<?php 
	// Error message, no number match found in the database. Flag Number to user and give alert options
	if(isset($error_noMatch) && $error_noMatch == true):
		echo '<ul class="admin_menu">
				<li class="button">
					<a href="alarm_screen.php?num='.$_GET['num_set_one'].'-'.$_GET['num_set_two'].'-'.$_GET['num_set_three'].'">'.$btn_confirm_the_number_on_voucher_is_correct.'</a>
				</li>
			  </ul>';
	endif;
	
	// Check to see if a match has been found. options: true / false.
	if(isset($match_found) && $match_found == true):
		echo '<h2>'.str_replace('{email}', $result['email'],$rmsg_one).'</h2><br/>';        
        
        
		// check to see if voucher redeemed: options: true / false.
		if(isset($redeemed) && $redeemed == false):
						
		
			echo $rmsg_six;
			echo '<br><br>';
			
			echo '<ul class="admin_menu">
					<li class="button">
						<a href="search_screen.php">'.$btn_no.'</a>
					</li>
					<li class="button">
						<a href="redeem_screen.php?num='.$_GET['num_set_one'].'-'.$_GET['num_set_two'].'-'.$_GET['num_set_three'].'&email='.$result['email'].'">'.$btn_yes.'</a>
					</li>
				  </ul>';
				

		endif;
	endif;
	
	
?>
</div>
</body>
</html>
