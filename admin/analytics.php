<?php
// Inialize session
session_start();


// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['username'])) {
        header('Location: index.php');
}


	// database analytics screen
	
	// Include database connection class
	require_once ('../classes/dbclass.php');
	
	// Include language config < edit this file to change language
	require_once ('../language_config.php.ini');
	
	$redeem_totla = '';
	$issued_totla = '';
	$OXF170_totla ='';
	$OXF311_totla = '';
	$WESTFIELDS_totla = '';
	$WESTFIELDL_totla = '';
	$BLUEWATER_totla =  '';
	$REGENTSTREET_totla = '';
	
	// Create a new db instance.
	$db = new Database();
	// make query on the db
	$db->query("SELECT COUNT(id) AS redeem_totla FROM voucher WHERE redeem = 1 AND channel IS NOT NULL");	
	// return the db results.
	$result = $db->getResult();	
	
	if(!$result['redeem_totla'] == 0):
		$redeem_totla = $result['redeem_totla'];
	endif;
	
	
	
	$db->query("SELECT COUNT(id) AS totla_issued FROM voucher WHERE channel IS NOT NULL");	
	// return the db results.
	$totla_issued_result = $db->getResult();
	
	if(!$totla_issued_result['totla_issued'] == 0):
		$issued_totla = $totla_issued_result['totla_issued'];
	endif;
	


	$db->query("SELECT COUNT(id) AS oxf170_issued FROM voucher WHERE redeem_by = '170 OXFORDSTREET'");	
	// return the db results.
	$OXF170_result = $db->getResult();
	
	if(!$OXF170_result['oxf170_issued'] == 0):
		$OXF170_totla = $OXF170_result['oxf170_issued'];
	endif;
	

	$db->query("SELECT COUNT(id) AS oxf311_issued FROM voucher WHERE redeem_by = '311OXFORD STREET'");	
	// return the db results.
	$OXF311_result = $db->getResult();
	
	if(!$OXF311_result['oxf311_issued'] == 0):
		$OXF311_totla = $OXF311_result['oxf311_issued'];
	endif;
	
	

	$db->query("SELECT COUNT(id) AS wests_issued FROM voucher WHERE redeem_by = 'WESTFIELDSTRATFORD'");	
	// return the db results.
	$WESTFIELDS_result = $db->getResult();
	
	if(!$WESTFIELDS_result['wests_issued'] == 0):
		$WESTFIELDS_totla = $WESTFIELDS_result['wests_issued'];
	endif;
	
	

	$db->query("SELECT COUNT(id) AS westl_issued FROM voucher WHERE redeem_by = 'westfieldlondon'");	
	// return the db results.
	$WESTFIELDL_result = $db->getResult();
	
	
	
	if(!$WESTFIELDL_result['westl_issued'] == 0):
		$WESTFIELDL_totla = $WESTFIELDL_result['westl_issued'];
	endif;
	$WESTFIELDL_totla;
	

	$db->query("SELECT COUNT(id) AS blue_issued FROM voucher WHERE redeem_by = 'BLUEWATER'");	
	// return the db results.
	$BLUEWATER_result = $db->getResult();
	
	if(!$BLUEWATER_result['blue_issued'] == 0):
		$BLUEWATER_totla = $BLUEWATER_result['blue_issued'];
	endif;
	

	$db->query("SELECT COUNT(id) AS reg_issued FROM voucher WHERE redeem_by = 'REGENTSTREET'");	
	// return the db results.
	$REGENTSTREET_result = $db->getResult();
	
	if(!$REGENTSTREET_result['reg_issued'] == 0):
		$REGENTSTREET_totla = $REGENTSTREET_result['reg_issued'];
	endif;
	
	
	

	// issued by channel
	$db_channel2 = new Database();
	$db_channel2->query("SELECT count(id) AS channle2issued FROM voucher WHERE channel =2 AND email IS NOT NULL");	
	
	$issued_by_channel2_result = $db_channel2->getResult();

	
	
	$db_channel6 = new Database();
	$db_channel6->query("SELECT count(id) AS channle6issued FROM voucher WHERE channel =6 AND email IS NOT NULL");	
	
	$issued_by_channel6_result = $db_channel6->getResult();

	
	
	$db_channel7 = new Database();
	$db_channel7->query("SELECT count(id) AS channle7issued FROM voucher WHERE channel =7 AND email IS NOT NULL");	
	
	$issued_by_channel7_result = $db_channel7->getResult();

	
	$db_channel8 = new Database();
	$db_channel8->query("SELECT count(id) AS channle8issued FROM voucher WHERE channel =8 AND email IS NOT NULL");	
	
	$issued_by_channel8_result = $db_channel8->getResult();
	
	$db_channel10 = new Database();
	$db_channel10->query("SELECT count(id) AS channle10issued FROM voucher WHERE channel =10 AND email IS NOT NULL");	
	
	$issued_by_channel10_result = $db_channel10->getResult();


	
	
	// redeemed by channel
	$db_channel2_reemed = new Database();
	$db_channel2_reemed->query("SELECT COUNT(id) AS db_channel2_reemed FROM voucher WHERE channel = 2 AND redeem = 1");
	$channel2_reemed = $db_channel2_reemed->getResult();

	
	$db_channel6_reemed = new Database();
	$db_channel6_reemed->query("SELECT COUNT(id) AS db_channel6_reemed FROM voucher WHERE channel = 6 AND redeem = 1");
	$channel6_reemed = $db_channel6_reemed->getResult();

	
	$db_channel7_reemed = new Database();
	$db_channel7_reemed->query("SELECT COUNT(id) AS db_channel7_reemed FROM voucher WHERE channel = 7 AND redeem = 1");
	$channel7_reemed = $db_channel7_reemed->getResult();

	
	$db_channel8_reemed = new Database();
	$db_channel8_reemed->query("SELECT COUNT(id) AS db_channel8_reemed FROM voucher WHERE channel = 8 AND redeem = 1");
	$channel8_reemed = $db_channel8_reemed->getResult();
	
	
	$db_channel10_reemed = new Database();
	$db_channel10_reemed->query("SELECT COUNT(id) AS db_channel10_reemed FROM voucher WHERE channel = 10 AND redeem = 1");
	$channel10_reemed = $db_channel10_reemed->getResult();
	
	
	$hstp_db = new Database();
	$hstp_db->query("SELECT * FROM heatstops");
	$heatstops = $hstp_db->getResult();

	
?>

<html>

<head>
  <title><?php echo $page_title_users;?></title>
  <link id="admin-stylesheet" rel="stylesheet" href="../css/styles.css" type="text/css" />
</head>

<body>
	<!-- admin bar html here -->		
	
	<?php 
		if($_SESSION['roles'] == 'admin'):
			$sp_menu = '&nbsp;|&nbsp; <a href="users.php">'.$btn_user.'</a>&nbsp;|&nbsp; <a href="channel_quota.php">'.$btn_quota.'</a>';
		else:
			$sp_menu = '';
		endif; 
	?>
	<!-- admin bar html here -->	
	<div class="admin_bar">
		<p><?php echo $wmsg_one; ?> <b><?php echo $_SESSION['username']; ?></b>&nbsp;|&nbsp; <a href="logout.php"><?php echo $btn_logout; ?></a>&nbsp;|&nbsp; <a href="search_screen.php"><?php echo $btn_search; ?></a><?php echo $sp_menu;?></p>
	</div>

	
	
	<div class="logo">
		<img src="img/uniqlo_logo.gif" width="940" height="256" alt="Uniqlo" />
	</div>
	
	<div class="content">
		<table style="margin: 10px auto 60px auto;" align="center" border=0 width="50%">
			<tr>
				<th>Total issued:</th>
				<th>Total redeemed:</th>
			</tr>
			<tr>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo '<strong>'.$issued_totla.'</strong>';?></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo '<strong>'.$redeem_totla.'</strong><br/>';?></td>
			</tr>
		</table>




		<h1>Total redeemed by store</h1>
		
		<table style="margin: 10px auto 60px auto; width: 70%;" align="center" border="1" >
			<tr>
				<th style="padding: 10px;">170 OXFORD STREET</th>
				<th style="padding: 10px;">311 OXFORD STREET</th>
				<th style="padding: 10px;">WESTFIELD STRATFORD</th>
				<th style="padding: 10px;">WESTFIELD LONDON</th>
				<th style="padding: 10px;">BLUEWATER</th>
				<th style="padding: 10px;">REGENT STREET </th>
			</tr>
			<tr>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $OXF170_totla;?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $OXF311_totla;?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $WESTFIELDS_totla;?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $WESTFIELDL_totla;?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $BLUEWATER_totla;?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $REGENTSTREET_totla;?><br/></td>
			</tr>
		</table>
		
		<h1>Total vouchers issued by channel</h1>
		
		<table style="margin: 10px auto 60px auto; width: 70%;" align="center" border="1" >
			<tr>
				<th style="padding: 10px;">Heatspots</th>
				<th style="padding: 10px;">Mobile Game</th>
				<th style="padding: 10px;">iPad App</th>
				<th style="padding: 10px;">Facebook Game</th>
				<th style="padding: 10px;">Printed Voucher</th>
			</tr>
			<tr>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $issued_by_channel2_result['channle2issued'];?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $issued_by_channel6_result['channle6issued'];?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $issued_by_channel7_result['channle7issued'];?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $issued_by_channel8_result['channle8issued'];?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $issued_by_channel10_result['channle10issued'];?><br/></td>
			</tr>
		</table>
		
		<h1>Total vouchers redeemed by channel</h1>
		
		<table style="margin: 10px auto 60px auto; width: 70%;" align="center" border="1" >
			<tr>
				<th style="padding: 10px;">Heatspots</th>
				<th style="padding: 10px;">Mobile Game</th>
				<th style="padding: 10px;">iPad App</th>
				<th style="padding: 10px;">Facebook Game</th>
				<th style="padding: 10px;">Printed Voucher</th>
			</tr>
			<tr>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $channel2_reemed['db_channel2_reemed'];?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $channel6_reemed['db_channel6_reemed'];?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $channel7_reemed['db_channel7_reemed'];?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $channel8_reemed['db_channel8_reemed'];?><br/></td>
				<td align="center" style="color: red; font-size: 1.5em;"><?php echo $channel10_reemed['db_channel10_reemed'];?><br/></td>
			</tr>
		</table>
		
		<h1>HeatSpot Totals</h1>

		<table style="margin: 10px auto 60px auto; width: 70%;" align="center" border="1" >
			<?php
				echo '<tr>';
				echo '<th>Heatspots</th>';
				echo '<th>Footsteps</th>';
				echo '<th>Joules</th>';
				echo '</tr>';
				foreach($heatstops as $items):
					echo '<tr>';
					echo '<td style="color: black; font-size: 1.5em;">'.$items['Name'].'</td>';
					echo '<td style="color: red; font-size: 1.5em;">'.$items['footsteps'].'</td>';
					echo '<td style="color: red; font-size: 1.5em;">'.$items['Kj'].'</td>';
					echo '</tr>';
				endforeach;
			?>
			
		</table>

				
	</div>
</body>
</html>


