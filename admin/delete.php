<?php
/************************************************************\
 *
 *   PHP Script to delete admin users Copyright 2012 
 *	  
 *
 *
\************************************************************/

// Inialize session
session_start();

// Check, if username session is NOT set then this page will jump to login page
if($_SESSION['roles'] == 'admin'):
	$user_role = true;
	
	require_once ('../classes/dbclass.php');
	// Create a new db instance.
	$db = new Database();
	
	if(!empty($_GET['id'])):
		$user_id = strip_tags(addslashes($db->clean($_GET['id'])));
		// Include database connection class		
		$db->query("DELETE  FROM users WHERE id='$user_id'");
	endif;
	
	header('Location: users.php');
		
else:
	header('Location: index.php');
endif;

?>