<?php
/************************************************************\
 *
 *   PHP Script redeem admin screen Copyright 2012 
 *	  
 *   Description:
 *   This script has the following.
 *
 *   1) session_start() // Start a session.
 *   2) header() // Redirect the header to the login page after logout.
 *   3) DateTime() // Date Time class.
 *   4) date_default_timezone_set('Europe/London') // Set date time zone.
 *
 *
\************************************************************/
// Include language config < edit this file to change language
require_once ('../language_config.php.ini');
// Inialize session
session_start();
// Check, if username session is NOT set then this page will jump to login page
if (!isset($_SESSION['username'])):
    header('Location: index.php');
endif;

// Include database connection class
require_once ('../classes/dbclass.php');

// Raise the alarm as this voucher number is not valid

if(isset($_GET['num']) && isset($_GET['email'])):	



        // Create a new db instance.
	$db = new Database();
        
        // values to update the db with.
	$vcode = preg_replace('/-/','', $_GET['num']); //Strip - from the voucher number	

	// set the time zone
	date_default_timezone_set('Europe/London'); // Add default time zone to fix warning maeesage.
	// Create DateTime instance.
	$date = new DateTime();
	$time_stamp = $date->format('Y-m-d H:i:s');
        
        
        
        $vcode = strip_tags(addslashes($db->clean($vcode)));
        
        $email = strip_tags(addslashes($db->clean($_GET['email'])));
        
        $time_stamp = strip_tags(addslashes($db->clean($time_stamp)));
        
        $user_name = strip_tags(addslashes($db->clean($_SESSION['username'])));

        
        
	$db->query("UPDATE voucher SET redeem= '1', redeem_date= '$time_stamp', redeem_by = '$user_name' WHERE vcode='$vcode' AND email='$email'");
	

        // set $redeemed to true, so we know to echo the message on screen.
	$redeemed = true;

else:
	echo 'error redirect';
	header('Location: logout.php');
endif;
?>

<html>

<head>
  <title><?php echo $page_title_redeem;?></title>
  <link id="admin-stylesheet" rel="stylesheet" href="../css/styles.css" type="text/css" />
</head>

<body>
	<!-- admin bar html here -->	
	<div class="admin_bar">
		<p><?php echo $wmsg_one; ?> <b><?php echo $_SESSION['username']; ?></b>&nbsp;|&nbsp; <a href="search_screen.php"><?php echo $btn_search_new;?></a> &nbsp;|&nbsp; <a href="logout.php"><?php echo $btn_logout; ?></a></p>
	</div>
	<div class="logo">
		<img src="img/uniqlo_logo.gif" width="940" height="256" alt="Uniqlo" />
	</div>
	
	<div class="content">
		<?php
		if(isset($redeemed) && $redeemed == true):
			
			echo str_replace('{VAR}', $_GET['num'],$rmsg_seven);
			echo '<br><br>';
			echo '<ul class="admin_menu">
					<li class="button">
						<a href="search_screen.php">'.$btn_search_new.'</a>
					</li>
				  </ul>';
		endif;
		?>
	</div>
</body>
</html>