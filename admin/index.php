<?php
/************************************************************\
 *
 *   PHP Script to login to the admin area Copyright 2012 
 *	  
 *   Description:
 *   This script has the following.
 *
 *   1) require_once() // Include the db class
 *   1) session_start() // Start a session.
 *   2) isset($_SESSION) // Check if the session has been set.
 *   3) query() // Guery DB to check usename and password has a match.
 *
 *
\************************************************************/
	// Include language config < edit this file to change language
	require_once ('../language_config.php.ini');
	
	
	// If the post variables have not been set, then set them to false
	if(!isset($_POST["username"])):
		$_POST['username'] = false;
	endif;
	
	if(!isset($_POST["password"])):
		$_POST['password'] = false;
	endif;
	
	
	// Include database connection class
	require_once ('../classes/dbclass.php');
	
	// Create a new db instance.
	$db = new Database();
	
	// start a session
	session_start();
	
	// If no user session found then exit, with error message.
	if(!isset($_SESSION['username'])) :
		// Retrieve username and password from database according to user's input
		$db->query("SELECT * FROM users WHERE (username = '" . addslashes($_POST['username']) . "') and (password = '" . addslashes(md5($_POST['password'])) . "')"); 
		
		// return the db results.
		$login = $db->getResult();
		
		// Check username and password match
		// Check to see if the user array has been set.
		if(isset($login["password"])):		
			// Double-check that the passwords are the same!
			if(addslashes(md5($_POST['password'])) == $login["password"]):
				// 'Lets redirect as the user is good to go.';
				// Set username session variable
		        $_SESSION['username'] = $_POST['username'];
				$_SESSION['roles'] = $login["roles"];
		        // Jump to secured page
		        header('Location: search_screen.php');
			endif;
		else:
			// check to see that the form has been submited and if no user password match print error message.			
			if (isset($_POST['submit'])):			    	
					$login_error = true;
			endif;
		endif;	
	endif;
?>
	
	
	<html>
		<head>
			<title><?php echo $page_title_login; ?></title>
			<link id="admin-stylesheet" rel="stylesheet" href="../css/styles.css" type="text/css" />
		</head>
		<body onload="document.login.username.focus();">
			<div class="logo">
				<img src="img/uniqlo_logo.gif" width="940" height="256" alt="Uniqlo" />
			</div>
			<div class="content">
			<h1><?php echo $page_title_login; ?></h1>
			
				<?php
					if(isset($login_error) && $login_error == true):
						echo '<br/><p>Sorry but the username or password do not match our records.<br/> Please try again.</p><br/>';
					endif;
				?>
			
			<form name="login" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>" >
				<legend style="display: none;"><?php echo $legend_login; ?></legend>
				<p>
				<label for="username">
					<input tabindex="1" accesskey="u" id="username" type="text" name="username" />
				<label>
				</p>
				<p>
				<label for="password">
					<input tabindex="2" accesskey="p" id="password" type="password" name="password" />
				<label>
				</p>
				<input class="form_button" name="submit" tabindex="3" accesskey="s" type="submit" value="<?php echo $btn_login; ?>">
			</form>
			</div>
		</body>
	</html>
	
	
