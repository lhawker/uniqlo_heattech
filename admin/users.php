<?php
// Include database connection class
require_once ('../language_config.php.ini');

// Inialize session
session_start();

// Check, if username session is NOT set then this page will jump to login page
if($_SESSION['roles'] == 'admin'):
	$user_role = true;
	// Include database connection class
	require_once ('../classes/dbclass.php');
	// Create a new db instance.
	$users_db = new Database();	
	$users_db->query("SELECT * FROM users WHERE roles='user'");
	$result = $users_db->getResult();

else:
	header('Location: index.php');
endif; 

?>


<html>

<head>
  <title><?php echo $page_title_users;?></title>
  <link id="admin-stylesheet" rel="stylesheet" href="../css/styles.css" type="text/css" />
</head>

<body>
	<!-- admin bar html here -->	
	<div class="admin_bar">
		<p><?php echo $wmsg_one; ?> <b><?php echo $_SESSION['username']; ?></b>&nbsp;|&nbsp; <a href="logout.php"><?php echo $btn_logout; ?></a>&nbsp;|&nbsp; <a href="search_screen.php"><?php echo $btn_search_new;?></a> &nbsp;|&nbsp; <a href="channel_quota.php"><?php echo $btn_quota; ?></a></p>
	</div>
	
	
	<div class="logo">
		<img src="img/uniqlo_logo.gif" width="940" height="256" alt="Uniqlo" />
	</div>
	
	<div class="content">
		<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
		<table align="cenetr" border="0" width="600px" cellspacing="10" cellpadding="10" style="border-collapse:collapse; margin:0 auto 20px auto;">
		<?php
			if(is_array($result)&& count($result) > 0):
				foreach($result as $val):
					
					$id = $val['id'];
					echo '<tr>';
					
					echo '<td>ID: '.$val['id'].'&nbsp;</td>';					
					
					echo '<td>'.$rmsg_eight.': <strong>'.$val['username'].'</strong>&nbsp;</td>';
					
					
					echo '<td>'.$rmsg_nine.': '.$val['roles'].'&nbsp;</td>';
					
					echo '<td><ul class="admin_menu">
							<li class="button">
								<a href="password.php?id=' . $val['id'] .'&username='. $val['username'] .'">'.$btn_search_pass.'</a>
							</li>
							<li class="button">
								<a href="delete.php?id=' . $val['id']. '">'.$btn_search_del.'</a>
							</li>
						  </ul></td>';
					
					
					
					echo '</tr>';		
				endforeach;
			endif;
		?>
		</table>
		

		   
		</form>		
	</div>
</body>
</html>