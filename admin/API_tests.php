<?php
// Inialize session
session_start();

// Check, if username session is NOT set then this page will jump to login page
if($_SESSION['roles'] == 'admin'):
	$user_role = true;
	// Include database connection class
	require_once ('../classes/dbclass.php');
	// Create a new db instance.
	$users_db = new Database();	
	$users_db->query("SELECT * FROM users WHERE roles='user'");
	$result = $users_db->getResult();

else:
	header('Location: index.php');
endif; 
//error_reporting(E_ALL); 
//ini_set( 'display_errors','1');
// report the channel status
	// include database connection class
	require_once ('../classes/dbclass.php');
	
	// include PHPMailer class
	require_once ('../phpmailer/class.phpmailer.php');	
	
	// Include database connection class
	require_once ('../language_config.php.ini');
	
	// include PHPMailer class
	require_once ('../functions.php');
	
?>


<!DOCTYPE html>
<html>
	<head>
    <title>API Tests</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<script type="text/javascript" src="../syntaxhighlighter_3.0.83/scripts/shCore.js"></script>
	<script type="text/javascript" src="../syntaxhighlighter_3.0.83/scripts/shBrushJScript.js"></script>
	<link type="text/css" rel="stylesheet" href="../syntaxhighlighter_3.0.83/styles/shCoreDefault.css"/>
	
	<script type="text/javascript">SyntaxHighlighter.all();</script>
	</head>
	<body>
<a style="float: left; margin-left: 10px;" href="search_screen.php">Back</a><a href="logout.php" style="float: right; margin-right: 10px;" href="">Logout</a>
		<h1 style="clear: both; padding-top: 20px;">Test API JSON FEEDS</h1>
		<ul>
			<li><a href="../channel_status_json.php">Channel status json feed.</a></li>
			<li><a href="../instoredata_json.php">Instore data json feed.</a></li>
			<li><a href="../voucher_form.html">Sign up test form.</a></li>
		</ul>
		<div id="channel_interactions">
			<h1>Channel interactions test:</h1>
			<span style="color: #999">
				Update mysql function used:<br/>
				<pre class="brush: js;">$db->update("UPDATE social_counts SET total = (SELECT total FROM (SELECT total as total FROM social_counts WHERE cid='$channel_code') AS tmptable)+$interactions, kj = (SELECT kj FROM (SELECT kj as kj FROM social_counts WHERE cid='$channel_code') AS tmptabl)+$kj WHERE cid='$channel_code'");</pre>
			</span>
			
			<form action="../channel_interactions.php" method="POST">
				<p><label for="channel"> Valid channels: 
					<select name="channel"  id="channel">
						<option value="1">id: 1, channel type: Interactions</option>
						<option value="2">id: 2, channel type: Heatstops</option>
						<option value="3">id: 3, channel type: Web</option>
						<option value="4">id: 4, channel type: Twitter</option>
						<option value="5">id: 5, channel type: Facebook Likes</option>
						<option value="6">id: 6, channel type: Mobile</option>
						<option value="7">id: 7, channel type: App</option>
						<option value="8">id: 8, channel type: Facebook Game</option>
					</select>
				<label></p>
				<p><label for="interactions"> Number of interactions: 
					<select name="interactions"  id="interactions">
					<?php 
						$i = 1;
						while ($i <= 100) {
							echo '<option value="'.$i++.'">' . $i++ . '</option>';
						}
					?>
					</select>
				<label></p>
					<p><label for="kj"> KJ value:
					<input id="kj" type="text" maxlength="3" name="kj" size="3" />
				<label></p>
				<p><input type="submit" value="submit"/></p>
			</form>
			
			<div style="background: #ccc;">
				<?php
					$db = new Database();
					$db->query("SELECT * FROM social_counts");
					$social_counts = $db->getResult();
										
					foreach($social_counts as $item):
						echo '<span style="color: #999;">Channel id: </span>'. $item["cid"].' <span style="color: #999;">Total interactions: </span>'. $item["total"].' <span style="color: #999;">KJ: </span>'. $item["kj"].'<br/>';
					endforeach;
				?>
			</div>
		</div>
		
<!-- camera test -->
		<div id="camera_switch">
			<h1>Switch in-store cameras test: </h1>
			<span style="color: #999">
				Update mysql function used:<br/>
				<pre class="brush: js;">$db->update("UPDATE heatstops SET Feed = $camera_switch WHERE id = $channel");</pre>
			</span>
			<form action="../camera_switch.php" method="POST">
				<p><label for="switch"> Switch camera: 
					<select name="switch"  id="switch">
						<option value="1">On</option>
						<option value="0">Off</option>
					</select>
				<label></p>
				<p><label for="hcode"> Valid channels: 
					<select name="hcode"  id="hcode">
						<option value="1">SOHO SQUARE</option>
						<option value="2">BLUEWATER</option>
						<option value="3">WESTFIELD WHITE CITY</option>
						<option value="4">WESTFIELD STRATFORD</option>
						<option value="5">OXFORD STREET</option>
						<option value="6">COVENT GARDEN</option>
					</select>
				<label></p>					
				<p><input type="submit" value="submit"/></p>
			</form>
			<div style="background: #ccc;">
				<?php
					$camera_switch_db = new Database();
					$camera_switch_db->query("SELECT * FROM heatstops");
					$camera_switch = $camera_switch_db->getResult();
									
					foreach($camera_switch as $item):
						echo '<span style="color: #999;">Location: </span>'. $item["Name"].' <span style="color: #999;">Feed: </span>'. $item["Feed"].'<br/>';
					endforeach;
				?>
			</div>
		</div>
		
<!-- footstep data test -->
		<div id="camera_switch">
			<h1>Update collected in-store footstep data test:  </h1>
			<span style="color: #999">
				Update mysql function used:<br/>
				<pre class="brush: js;">$db->update("UPDATE heatstops SET Footsteps = (SELECT Footsteps FROM (SELECT Footsteps as Footsteps FROM heatstops WHERE id=$hcode) AS tmptable)+$footsteps, Kj = (SELECT Kj FROM (SELECT Kj as Kj FROM heatstops WHERE id=$hcode) AS tmptable)+$kj WHERE id=$hcode");</pre>
			</span>
			<form action="../footstep_insert.php" method="POST">
				<p><label for="footsteps"> Collected footstep: 
					<select name="footsteps"  id="footsteps">
						<?php 
						$i = 0;
						while ($i <= 500) {
							echo '<option value="'.$i++.'">' . $i++ . '</option>';
						}
					?>
					</select>
				<label></p>
					<p><label for="kj"> Total KJ: 
					<select name="kj"  id="kj">
						<?php 
						$i = 0;
						while ($i <= 3500) {
							echo '<option value="'.$i++.'">' . $i++ . '</option>';
						}
					?>
					</select>
				<label></p>	
				<p><label for="hcode"> Valid channels: 
					<select name="hcode"  id="hcode">
						<option value="1">SOHO SQUARE</option>
						<option value="2">BLUEWATER</option>
						<option value="3">WESTFIELD WHITE CITY</option>
						<option value="4">WESTFIELD STRATFORD</option>
						<option value="5">OXFORD STREET</option>
						<option value="6">COVENT GARDEN</option>
					</select>
				<label></p>			
				
				<p><input type="submit" value="submit"/></p>
			</form>
			<div style="background: #ccc;">
				<?php
					$footstep_insert_db = new Database();
					$footstep_insert_db->query("SELECT * FROM heatstops");
					$footstep_insert = $footstep_insert_db->getResult();
		
					foreach($footstep_insert as $item):
						echo '<span style="color: #999;">Location: </span>'. $item["Name"].' <span style="color: #999;">Total footsteps: </span>'. $item["footsteps"].' <span style="color: #999;">KJ: </span>'. $item["Kj"].'<br/>';
					endforeach;
				?>
			</div>
			
<!-- Main functions-->
		<div id="main_functions">
			<h1>Main PHP functions behind the system: </h1>
			<span style="color: #999">
				Functions:<br/>
				<pre class="brush: js;">
	/************************************************************\
   *
   *   PHP Script containing all the functions Copyright 2012 
   *	  
   *
   *   This script has the following functions
   *
   *   1) update_voucher // insert users data into the db function
   *   2) splitVcode // function to splite the voucher number into groups
   *   3) admin_email // Admin email to notify admin users about system updates
   *   4) user_email // User email notification, used to send voucher to user.
   *
   *   Description:
   *
   *
   *
  \************************************************************/
	/**
    * get facebook likes and calculate kj used update db with final values
    * @param $url string 
	* @param $channel string 
	*
	* Description pass a facebook url value to $url. pass the channel id to $channel
    * 
    */

	function count_facebook($channel, $url) {
		
		$html = file_get_contents($url);
		
		if(isset($html)):
			$likes = json_decode($html, true);
			$likes_total = $likes['uniqlo.uk']['likes'];
			$energy_perclick = ceil(0.0000016743); // kj per click
			$energy = $energy_perclick * $likes_total;
			
			$db = new Database();
			// update the db with these new values
			$db->update("UPDATE social_counts SET total=$likes_total , kj=$energy WHERE cid=$channel");			
			return $likes_total;
		else:
			return false;
			
		endif;		
	}

  /**
    * insert twitter data count into the db function
    * @param $hash_tag string 
	*
	* Description pass a hash tag value to $hash_tag
    * 
    */

	function count_tweets($hash_tag) {
		
		$db = new Database();
		$db->query("SELECT since_id FROM tw_since_id");	
		$sid_array = $db->getResult();

		if(!empty($sid_array)):
			$since_id = $sid_array['since_id'];
		else:
			$since_id = '256370717849972736';
		endif;
	
		$jsonurl = 'http://search.twitter.com/search.json?q='.urlencode($hash_tag).'&since_id='.$since_id.'&page=1&rpp=100';		
			
		$json = file_get_contents($jsonurl,0,null,null);
		
				
		if (!$json):
        	echo "ERROR: Twitter Stream Error: failed to open stream";
		else:

			$json_output = json_decode($json);
			
			/* echo '<h1>'.count($json_output->results).'</h1><hr/>';
			var_dump($json_output); // uncomment for testing */
									
			$s_id = explode("=", $json_output->refresh_url);
			$s_idValue = str_replace("&q", "", $s_id[1]);
			
			
			$db_t = new Database();
			$db_t->query("SELECT total FROM social_counts WHERE cid='4' ");
			$twtotal_array = $db_t->getResult();				

			$db->update("UPDATE tw_since_id SET since_id = '$s_idValue' WHERE id='1'");
			
			$sum = count($json_output->results);
			
			if($sum > 0):
				$tweet_kj = 1;
			else:
				$tweet_kj = 0;
			endif;

			$new_total = $twtotal_array['total'];		
			
			//$db->update("UPDATE social_counts SET total = (SELECT total FROM (SELECT total as total FROM social_counts WHERE cid='4') AS tmptable)+$sum WHERE cid='4'");
			
			$db->update("UPDATE social_counts SET total = (SELECT total FROM (SELECT total as total FROM social_counts WHERE cid='4') AS tmptable)+$sum, kj = (SELECT kj FROM (SELECT kj as kj FROM social_counts WHERE cid='4') AS tmptable)+$tweet_kj WHERE cid='4'");
			
			return $new_total;			
		endif;		
	}



  /**
    * insert users data into the db function
    * @param $email string
    * @param $channel_code string
    * 
    */
	function update_voucher($channel_code, $firstname, $lastname, $gender, $email, $opt_in){
		// echo 'updated'; // << Uncomment for testing.
		$db = new Database();
		
		$db->update("UPDATE voucher SET channel = '$channel_code', firstname = '$firstname', lastname = '$lastname', gender = '$gender', email= '$email' , opt_in = '$opt_in' WHERE vcode = (SELECT vcode FROM (SELECT MIN(vcode) as vcode FROM voucher WHERE channel IS NULL) AS tmptable) AND channel IS NULL");
		
	}
	
	/**
	  * Splite the voucher number into groups
	  * @param $vcode_str string
	  * 
	  */	
	function splitVcode($vcode_str) {
		$str_implode = str_split($vcode_str, 3);
		
		if(is_array($str_implode)):
			if(count($str_implode)==3):
			return $str_implode[0].'-'.$str_implode[1].'-'.$str_implode[2];
			endif;
		endif;
	}
	
	
	/**
	  * Close the channel if the total number of voucher per channel = max flag value
	  * @param $channel_code string
	  * 
	  */	
	function closeChannel($channel_code) {
		
		$db_cc = new Database();
		$db_cc->query("SELECT MAX(a.flag) AS flag, (SELECT count(b.channel) as channel from voucher as b where b.channel =$channel_code ) AS channel FROM channel_triggers as a WHERE a.cid=$channel_code");
		$channel_results = $db_cc->getResult();
		
		if(!empty($channel_results)):
			$channel_count = $channel_results['channel'];
			$flag_count = $channel_results['flag'];
			
			if($channel_count == $flag_count):
				$db_status = new Database();
				$db_status->update("UPDATE channels SET status=1 WHERE id=$channel_code");						
			endif;
		endif;
	}
	
	
	
	/* ************************	START OF EMAIL FUNCTIONS ********************************** */
	
	// admin email to notify admin users about system updates
	function admin_email($admin_message){

		// default system email address for sent emails
		//$system_email_address = 'info@uniqlo.co.uk';
		$system_email_address = 'noreply@uniqlo.com';
		$system_email_address = 'lawrence@lhawker.co.uk'; //< this email address is for testing only

		// get the admin user data from the database.	
		$db = new Database();
		$db->query("SELECT * FROM admin_emails"); 
		$result = $db->getResult();



			/* ///////////////////////////////////// START BUILD AND SEND THE EMAIL. */	

			$mail  = new PHPMailer(); // defaults to using php "mail()"
			$mail->IsSendmail(); // Use SendMail transport
			//$body  = file_get_contents('contents.html');
			
			$body  = file_get_contents('emails/admin.html');
			
			//$body  = $user_message;
			$body = str_replace('{ERROR}', $admin_message, $body);
			
			//$body  = $admin_message;

			$mail->AddReplyTo($system_email_address);
			$mail->SetFrom($system_email_address);			
			
			
			// loop over the admin users and send out the email to them.
			foreach($result as $user_array):
				$mail->AddAddress($user_array['email'], $user_array['username']);
			endforeach;
			
			
			$mail->Subject    = "UNIQLO voucher admin system update!";
			$mail->AltBody    = "$admin_message"; // optional, comment out and test

			$mail->MsgHTML($body);

			if(!$mail->Send()) {
			  $error_msg = 'Mailer Error';
			  //echo '{"SUCCESS":false, "CODE":6, "MESSAGE":'.json_encode($error_msg).'}';	
			} else {
			  $error_msg = 'Message sent!'; // < Dump the email array for testing.
			  //echo '{"SUCCESS":true, "CODE":6, "MESSAGE":'.json_encode($error_msg).'}';	
			}

			/* ///////////////////////////////////// END BUILD AND SEND THE EMAIL. */	

	}
	
	// User email notification, used to send voucher to user.
	function user_email($firstname, $lastname, $email, $user_message){
		
		// default system email address for sent emails
		//$system_email_address = 'info@uniqlo.co.uk';
		
		$system_email_address = 'noreply@uniqlo.com';
		
		
		
		
		$member_name = $firstname.' '.$lastname;

		/* ///////////////////////////////////// START BUILD AND SEND THE EMAIL. */	

			$mail  = new PHPMailer(); // defaults to using php "mail()"
			$mail->IsSendmail(); // Use SendMail transport
			$body  = file_get_contents('emails/user.html');
			
			//$body  = $user_message;
			//$body = str_replace('{MEMBER_NAME}', $member_name, $body);
			$body = str_replace('{MEMBER_VCODE}', $user_message, $body);
									
			$mail->AddReplyTo($system_email_address);
			$mail->SetFrom($system_email_address);
			$address = $email;

			$mail->AddAddress($address);
			$mail->Subject    = "Here is your UNIQLO voucher number:";
			$mail->AltBody    = 'Dear '.$member_name.', '.$user_message; // optional, comment out and test

			$mail->MsgHTML($body);

			if(!$mail->Send()) {
			  $error_msg = 'Mailer Error:';
			  echo '{"SUCCESS":false, "CODE":6, "MESSAGE":'.json_encode($error_msg).'}';		
			} else {
			  $error_msg =  'Message sent!'; // < Dump the email array for testing.
              echo '{"SUCCESS":true, "CODE":6, "MESSAGE":'.json_encode($error_msg).'}';	
			}

			/* ///////////////////////////////////// END BUILD AND SEND THE EMAIL. */	

	}
	
		/* ************************	END OF EMAIL FUNCTIONS ********************************** */
	
</pre>
			</span>
		</div>
		
	</body>
</html>