<?php
// edit the channel quota basebase table

// Include database connection class
require_once ('../language_config.php.ini');

// Inialize session
session_start();

// Check, if username session is NOT set then this page will jump to login page
if($_SESSION['roles'] == 'admin'):
	$user_role = true;
	// Include database connection class
	require_once ('../classes/dbclass.php');
	// Create a new db instance.
	$users_db = new Database();	
	$users_db->query("SELECT * FROM users WHERE roles='user'");
	$result = $users_db->getResult();

else:
	header('Location: index.php');
endif;

require_once ('../classes/dbclass.php');
// Create a new db instance.
$db = new Database();
$db->query("SELECT * FROM heatstops");

$result = $db->getResult();



if($_SERVER['REQUEST_METHOD'] == "POST"):

	$id = $_POST['id'];
	$Footsteps = $_POST['footsteps'];
	$Kj = $_POST['kj'];
	
	$db = new Database();
	$db->update("UPDATE heatstops SET Footsteps= $Footsteps, Kj = $Kj WHERE id = $id ");
	
	$url = $_SERVER['REQUEST_URI'];
	header('Location: '.$url);
	
endif;


function updateData(){
	// update data after post action
	echo 'form sent';
}

function parseArrayToObject($array) {
    $object = new stdClass();
    if (is_array($array) && count($array) > 0) {
        foreach ($array as $name=>$value) {
            $name = strtolower(trim($name));
            if (!empty($name)) {
                $object->$name = $value;
            }
        }
    }
    return $object;
}

?>

<html>

<head>
  <title><?php echo $page_title_users;?></title>
  <link id="admin-stylesheet" rel="stylesheet" href="../css/styles.css" type="text/css" />
</head>

<body>
	<!-- admin bar html here -->		
	
	<div class="admin_bar">
		<p><?php echo $wmsg_one; ?> <b><?php echo $_SESSION['username']; ?></b>&nbsp;|&nbsp; <a href="logout.php"><?php echo $btn_logout; ?></a> &nbsp;|&nbsp; <a href="search_screen.php"><?php echo $btn_search_new;?></a> &nbsp;|&nbsp; <a href="users.php"><?php echo $btn_user; ?></a></p>
	</div>
	
	
	
	<div class="logo">
		<img src="img/uniqlo_logo.gif" width="940" height="256" alt="Uniqlo" />
	</div>
	
	<div class="content">

<?php
	if(!empty($result)):
		
		echo '<table align="cenetr" border="0" width="500px" cellspacing="10" cellpadding="10" style="border-collapse:collapse; margin:0 auto 20px auto;">';
		echo '<tr>';
			echo '<th align="center">Location</th>';
			echo '<th align="center">Footsteps</th>';
			echo '<th align="center">Joules</th>';
			echo '<th align="center"></th>';
			echo '<th align="center"></th>';
		echo '<tr>';
		
		
		foreach($result as $item):
			$obj = parseArrayToObject($item);
			echo '<tr>';
			echo '<form action="'.$_SERVER['PHP_SELF'].'" method="post">';
			echo '<td align="center">'.$obj->name.'</td>';			
			echo '<td align="center"><input style="background: silver;" id="footsteps" type="text" name="footsteps"  value="'.$obj->footsteps.'" /></td>';			
			echo '<td><input id="kj" type="text" name="kj" size="12" value="'.$obj->kj.'" /></td>';						
			echo '<td align="center"><input id="id" type="hidden" name="id" size="12" value="'.$obj->id.'" /></td>';
				echo '<td><input name="submit" type="submit" value="Update"></td>';
				echo '</form>';
			echo '<tr>';
		endforeach;
		echo '</table>';
		
	endif;

?>

	</div>
</body>
</html>








